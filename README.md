# acrostic-gen

![](https://img.shields.io/badge/written%20in-PHP-blue)

Script to generate random acrostic gibberish.

The distribution zip file includes two wordlists; an english dictionary derived from Debian Jessie's `wamerican` package, and a list of business-oriented buzzwords and phrases.

## Example


```
$ acrostic-gen.php $(cat words-ame-filtered | shuf -n1) words-ame-filtered

          cAppuccinos
          tUcuman
       denoTe
         beHring
      insurEr
     caddyiNg
       demeTrius
     objectIvely
      proseCution
        tenAcious
promiscuousLy
    tranquiLlity
         noYce
```


## TODO

- Bias random word selection to favour what would more centrally-align the text
- Option to left- or right-align generated text
- Colourize text output if terminal is detected
- Create fancy motivational jpeg image
- Become millionaire via selling email subscriptions


## Download

- [⬇️ acrostic-gen_r01.zip](dist-archive/acrostic-gen_r01.zip) *(170.14 KiB)*
